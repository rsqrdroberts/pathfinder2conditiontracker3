import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../model/combat.dart';

class CombatWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Consumer<Combat>(
        builder: (ctx, combat, child) => Column(
          children: [
            Text("Combat's round ${combat.round.toString()}"),
            child!,
          ],
        ),
        child: ElevatedButton(
          child: Text('next round'),
          onPressed: () {
            print('Next Round button pressed');
            var combat = context.read<Combat>();
            combat.nextRound();
          },
        ),
      ),
    );
  }
}
