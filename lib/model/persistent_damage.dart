import './effect.dart';
import './combatant_listener.dart';
import './combatant.dart';

class PersistentDamage extends Effect {
  bool _damagedThisTurn = false;

  PersistentDamage({
    Combatant? onCombatant,
    Combatant? byCombatant,
  }) : super(
          onCombatant: onCombatant,
          byCombatant: byCombatant,
        ) {
    if (onCombatant == null) {
      // TODO: must have an onCombatant
    }
  }

  @override
  void added() {
    super.added();
    addCombatantListeners(
      onCL: CombatantListener(
        onBeginTurn: _onBeginTurn,
        onPersistentDamageAtDelay: _onPersistentDamageAtDelay,
        onPersistentDamageAtEnd: _onPersistentDamageAtEnd,
      ),
    );
  }

  void _onBeginTurn(Combatant _) {
    // clear out the persistent damage now that
    // a new turn has begun
    _damagedThisTurn = false;
  }

  void _onPersistentDamageAtDelay(Combatant c) {
    // TODO: notify of persistent damage
    _damagedThisTurn = true;
  }

  void _onPersistentDamageAtEnd(Combatant c) {
    // if the damage was already done due to a delay then
    // skip the persistent damage.
    if (_damagedThisTurn) {
      return;
    }
    // TODO: notify of persistent damage
    _damagedThisTurn = true;
  }
}
