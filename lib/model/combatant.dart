import 'package:flutter/foundation.dart';
import 'package:uuid/uuid.dart';

import './combat.dart';
import './combatant_type.dart';
import './combatant_state.dart';
import './combatant_listener.dart';

var _globalUuid = Uuid();

void _invoke(CombatantListenerCallback? cb, Combatant c) {
  if (cb == null) {
    return;
  }
  cb(c);
}

// TURNS, CRB pp. 468-469
// DELAY, CRB p. 470
class Combatant with ChangeNotifier {
  final String _uuid;
  final Combat _combat;
  String _name;
  CombatantType type;
  double initiative;
  CombatantState _state = CombatantState.BEFORE_FIRST_TURN;
  final List<CombatantListener> _listeners = [];

  Combatant({
    required combat,
    required name,
    required this.type,
    required this.initiative,
  })  : _combat = combat,
        _name = name,
        _uuid = _globalUuid.v4();

  Combatant.setState({
    required String uuid,
    required combat,
    required name,
    required this.type,
    required this.initiative,
    required state,
  })  : _combat = combat,
        _name = name,
        _state = state,
        _uuid = uuid;

  String get name => _name;

  set name(String name) {
    _name = name;
    notifyListeners();
  }

  Combat get combat => _combat;

  CombatantState get state => _state;

  String get uuid => _uuid;

  void addCombatantListener(CombatantListener listener) {
    _listeners.add(listener);
  }

  bool removeCombatantListener(CombatantListener listener) {
    return _listeners.remove(listener);
  }

  // TODO is this the way we want to handle notifications?
  void addMessages(List<String> messages) {
    _combat.addMessages(messages);
  }

  void beginTurn() {
    // check for state constraints
    if (!(_state == CombatantState.TURN_ENDED ||
        _state == CombatantState.BEFORE_FIRST_TURN)) {
      throw 'Invalid State Transition';
    }

    // notifiy listeners that the combatant's turn is about to begin
    _listeners.forEach(
      (listener) => _invoke(
        listener.onBeginTurn,
        this,
      ),
    );

    // If you created an effect lasting for a certain number of rounds,
    // decrement the number of rounds remaining by 1.
    _listeners.forEach(
      (listener) => _invoke(
        listener.onDecrementRemainingAtBegin,
        this,
      ),
    );

    // You can use 1 free action or reaction with a trigger of “Your turn
    // begins” or something similar.
    _listeners.forEach(
      (listener) => _invoke(
        listener.onTriggerAtBegin,
        this,
      ),
    );

    // If you’re dying, roll a recovery check
    _listeners.forEach(
      (listener) => _invoke(
        listener.onRecoveryCheckAtBegin,
        this,
      ),
    );

    // Do anything else that is specified to happen at the start of your turn,
    // such as regaining Hit Points from fast healing or regeneration
    _listeners.forEach(
      (listener) => _invoke(
        listener.onRegenerationAtBegin,
        this,
      ),
    );

    _state = CombatantState.TURN_BEGAN;

    notifyListeners();
  }

  void endTurn() {
    // check for state constraints
    if (_state != CombatantState.TURN_BEGAN ||
        _state != CombatantState.UNDELAYED) {
      throw 'Invalid State Transition';
    }

    // notifiy listeners that the combatant's turn is about to end
    _listeners.forEach(
      (listener) => _invoke(
        listener.onEndTurn,
        this,
      ),
    );

    // End any effects that last until the end of your turn.
    _listeners.forEach(
      (listener) => _invoke(
        listener.onEndEffectsAtEnd,
        this,
      ),
    );

    // Some effects caused by enemies might also last through a certain number
    // of your turns, and you decrease the remaining duration by 1 during this
    // step, ending the effect if its duration is decrementd to 0.
    _listeners.forEach(
      (listener) => _invoke(
        listener.onDecrementRemainingAtEnd,
        this,
      ),
    );

    // If you have a persistent damage condition, you take the damage at this
    // point. After you take the damage, you can attempt the flat check to end
    // the persistent damage
    _listeners.forEach(
      (listener) => _invoke(
        listener.onPersistentDamageAtEnd,
        this,
      ),
    );

    // You then attempt any saving throws for ongoing afflictions
    _listeners.forEach(
      (listener) => _invoke(
        listener.onAfflictionSaveAtEnd,
        this,
      ),
    );

    // Many other conditions change at the end of your turn, such as the
    // frightened condition decreasing in severity. These take place after
    // you’ve taken any persistent damage, attempted flat checks to end the
    // persistent damage, and attempted saves against any afflictions.
    _listeners.forEach(
      (listener) => _invoke(
        listener.onConditionChangeAtEnd,
        this,
      ),
    );

    // You can use 1 free action or reaction with a trigger of “Your turn
    // ends” or something similar.
    // Resolve anything else specified to happen at the end of your turn.
    _listeners.forEach(
      (listener) => _invoke(
        listener.onTriggerAtEnd,
        this,
      ),
    );

    _state = CombatantState.TURN_ENDED;

    notifyListeners();
  }

  void delay() {
    // check for state constraints
    if (_state != CombatantState.TURN_BEGAN) {
      throw 'Invalid State Transition';
    }

    // notifiy listeners that the combatant is about to be delayed
    _listeners.forEach(
      (listener) => _invoke(
        listener.onDelayed,
        this,
      ),
    );

    // When you Delay, any persistent damage or other negative effects that
    // normally occur at the start or end of your turn occur immediately when
    // you use the Delay action.
    _listeners.forEach(
      (listener) => _invoke(
        listener.onPersistentDamageAtDelay,
        this,
      ),
    );
    _listeners.forEach(
      (listener) => _invoke(
        listener.onNegativeEffectsAtDelay,
        this,
      ),
    );

    // Any beneficial effects that would end at any
    // point during your turn also end. The GM might determine that other
    // effects end when you Delay as well.
    _listeners.forEach(
      (listener) => _invoke(
        listener.onEndBeneficalEffectsAtDelay,
        this,
      ),
    );

    // Essentially, you can’t Delay to avoid negative consequences that would
    // happen on your turn or to extend beneficial effects that would end on
    // your turn.

    _state = CombatantState.DELAYED;

    notifyListeners();
  }

  void undelay() {
    // check for state constraints
    if (_state != CombatantState.DELAYED) {
      throw 'Invalid State Transition';
    }

    // notifiy listeners that the combatant is about to be undelayed
    _listeners.forEach(
      (listener) => _invoke(
        listener.onUndelayed,
        this,
      ),
    );

    _state = CombatantState.UNDELAYED;

    notifyListeners();
  }

  void removed() {
    notifyListeners();
  }
}
