import './effect.dart';
import './combatant.dart';
import './duration.dart';
import './effect_strategy.dart';

class Frightened extends Effect {
  Frightened({
    Combatant? onCombatant,
    Combatant? byCombatant,
    Duration? remaining,
    int value = 1,
  }) : super(
          onCombatant: onCombatant,
          byCombatant: byCombatant,
          remaining: remaining,
          value: value,
        ) {
    if (onCombatant == null) {
      // TODO: must have an onCombatant
    }
    if (value <= 0) {
      // TODO: must have a positive value
    }
  }

  @override
  void added() {
    super.added();

    var strat1 = DecrementDurationStrategy();
    strat1.set(this, _remainingExpired);
    var strat2 = ConditionChangeStrategy();
    strat2.set(this, _valueDecrementedToZero);
  }

  void _valueDecrementedToZero(Combatant c) {
    // TODO: implement valueDecrementedToZero
  }

  void _remainingExpired(Combatant c) {
    // TODO: implement _remainingExpired
  }
}
