import './effect.dart';
import './duration.dart';
import './combatant.dart';
import './combatant_listener.dart';
import './effect_strategy.dart';

class Affliction extends Effect {
  Duration? _totalRemaining;
  Duration? _maxDuration;
  Duration? _onsetRemaining;
  Duration? _onsetDuration;

  Affliction({
    Combatant? onCombatant,
    Combatant? byCombatant,
    Duration? remaining,
    int? value,
    Duration? maxDuration,
    Duration? onsetDuration,
  })  : _onsetDuration = onsetDuration,
        _maxDuration = maxDuration,
        super(
          onCombatant: onCombatant,
          byCombatant: byCombatant,
          remaining: remaining,
          value: value,
        ) {
    if (onCombatant == null) {
      // TODO: must have an onCombatant
    }
    if (value != null && value <= 0) {
      // TODO: must have a positive value
    }
    _totalRemaining = maxDuration != null ? maxDuration.copy() : null;
    _onsetRemaining = onsetDuration != null ? onsetDuration.copy() : null;
  }

  Duration? get totalRemaining => _totalRemaining;

  Duration? get onsetRemaining => _onsetRemaining;

  Duration? get maxDuration => _maxDuration;

  Duration? get onsetDuration => _onsetDuration;

  @override
  void added() {
    super.added();
    addCombatantListeners(
      onCL: CombatantListener(
        onAfflictionSaveAtEnd: _onAfflictionSaveAtEnd,
      ),
    );
  }

  void _onAfflictionSaveAtEnd(Combatant c) {
    // only do affliction if the combatant has
    // had a beginning of turn with this affliction
    if (!onHasBegunTurn) {
      return;
    }

    // if in onset then handle that...

    if (_onsetRemaining != null) {
      _decrementOnsetRemaing(c);
    } else {
      // decrement totalRemaining
      _decrementTotalRemaining(c);

      // then reduce the remaining for this stage
      var strategy = DecrementDurationStrategy();
      strategy.decrementRemaining(this, c, _remainingExpired);
    }

    notifyListeners();
  }

  void _decrementTotalRemaining(Combatant c) {
    var totRem = _totalRemaining;
    if (totRem != null) {
      totRem -= 1;
      _totalRemaining = totRem;
      if (totRem.isExpired) {
        _totalRemainingExpired(c);
      }
    }
  }

  void _decrementOnsetRemaing(Combatant c) {
    var onsetRemaining = _onsetRemaining;
    if (onsetRemaining == null) {
      return;
    }
    onsetRemaining -= 1;
    _onsetRemaining = onsetRemaining;
    if (onsetRemaining.isExpired) {
      _onsetRemainingExpired(c);
    }
  }

  void _remainingExpired(Combatant c) {
    // TODO: implement remainingExpired
  }

  void _totalRemainingExpired(Combatant c) {
    // TODO: notify that total duration has expired
  }

  void _onsetRemainingExpired(Combatant c) {
    // TODO: notify that onset has expired
  }
}
