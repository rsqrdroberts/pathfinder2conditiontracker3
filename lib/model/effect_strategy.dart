import './effect.dart';
import './combatant.dart';
import './combatant_listener.dart';

void _invoke(CombatantListenerCallback? cb, Combatant c) {
  if (cb == null) {
    return;
  }
  cb(c);
}

abstract class EffectStrategy {
  void set(
    Effect effect,
    CombatantListenerCallback? callback,
  ); // abstract method
}

class DecrementDurationStrategy implements EffectStrategy {
  @override
  void set(
    Effect effect,
    CombatantListenerCallback? remainingExpired,
  ) {
    Combatant? byComb = effect.byCombatant;

    // if byCombatant not null, then the byCombatant
    // decrements remaining at beginning of its turn,
    // otherwise onCombatant reduces at the end of its turn.

    var onCombatantDecrements = byComb == null;

    effect.addCombatantListeners(
        onCL: CombatantListener(
          onDecrementRemainingAtEnd: onCombatantDecrements
              ? (c) => decrementRemaining(
                    effect,
                    c,
                    remainingExpired,
                  )
              : null,
        ),
        byCL: CombatantListener(
          onDecrementRemainingAtBegin: onCombatantDecrements
              ? null
              : (c) => decrementRemaining(
                    effect,
                    c,
                    remainingExpired,
                  ),
        ));
  }

  void decrementRemaining(
    Effect effect,
    c,
    CombatantListenerCallback? remainingExpired,
  ) {
    // Do not reduce remaining for onCombatant
    // unless it has seen a beginning of turn with this effect
    if (c == effect.onCombatant && !effect.onHasBegunTurn) {
      return;
    }

    var r = effect.remaining;
    if (r != null) {
      r -= 1;
      effect.remaining = r;
      if (r.isExpired) {
        _invoke(
          remainingExpired,
          c,
        );
      }
    }
  }
}

class NegativeEffectStrategy implements EffectStrategy {
  @override
  void set(
    Effect effect,
    CombatantListenerCallback? doNegativeEffect,
  ) {
    effect.addCombatantListeners(
      onCL: CombatantListener(
        onNegativeEffectsAtDelay: doNegativeEffect,
      ),
    );
  }
}

class BeneficialEffectStrategy implements EffectStrategy {
  @override
  void set(
    Effect effect,
    CombatantListenerCallback? endBeneficialEffect,
  ) {
    effect.addCombatantListeners(
      onCL: CombatantListener(
        onEndBeneficalEffectsAtDelay: endBeneficialEffect,
      ),
    );
  }
}

class ConditionChangeStrategy implements EffectStrategy {
  @override
  void set(
    Effect effect,
    CombatantListenerCallback? valueDecrementedToZero,
  ) {
    effect.addCombatantListeners(
      onCL: CombatantListener(
        onConditionChangeAtEnd: (c) => decrementValue(
          effect,
          c,
          valueDecrementedToZero,
        ),
      ),
    );
  }

  void decrementValue(
    Effect effect,
    Combatant c,
    CombatantListenerCallback? valueDecrementedToZero,
  ) {
    // only reduce its value if it does not have a duration
    if (effect.remaining != null) {
      return;
    }

    var v = effect.value;
    if (v != null) {
      v = (v > 0) ? v - 1 : 0;
      effect.value = v;
      if (v == 0) {
        _invoke(valueDecrementedToZero, c);
      }
    }
  }
}
