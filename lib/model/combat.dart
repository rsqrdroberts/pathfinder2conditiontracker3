import 'package:flutter/foundation.dart';
import 'package:uuid/uuid.dart';

import './combatant_type.dart';
import './combatant.dart';
import './effect.dart';
import './frightened.dart';

var _globalUuid = Uuid();

class Combat with ChangeNotifier {
  final String _uuid;
  final List<Combatant> _combatants = [];
  final List<Effect> _effects = [];
  Combatant? _currentCombatant;
  Combatant? _inTurn;
  final List<Combatant> _delayedCombatants = [];
  final List<String> _messages = [];
  int _round = 0;

  Combat() : _uuid = _globalUuid.v4();

  Combat.setState({required String uuid}) : _uuid = uuid;

  String get uuid => _uuid;

  int get round => _round;

  Combatant? get inTurn => _inTurn;

  Combatant? get currentCombatant => _currentCombatant;

  List<Combatant> get combatants => [..._combatants];

  List<Combatant> get delayedCombatants => [..._delayedCombatants];

  List<Effect> get effects => _effects;

  void nextRound() {
    _round++;
    print("Combat.round: $round");

    notifyListeners();
  }

  // give it the info need to construct the Effect
  void addEffect(
    Combatant? onCombatant,
    Combatant? byCombatant,
  ) {
    // TODO (not really just Frightened)
    var effect = Frightened(
      onCombatant: onCombatant,
      byCombatant: byCombatant,
    );

    _effects.add(effect);
    effect.added();

    notifyListeners();
  }

  void removeEffect(Effect effect) {
    effect.removed();
    _effects.remove(effect);

    notifyListeners();
  }

  void addMessages(List<String> notifications) {
    _messages.addAll(notifications);

    notifyListeners();
  }

  List<String> get messages => [..._messages];

  void clearMessages() {
    _messages.clear();

    notifyListeners();
  }

  void sort() {
    // sort initiative higher to lower
    _combatants.sort(
      (c1, c2) => c2.initiative.compareTo(c1.initiative),
    );

    notifyListeners();
  }

  void addCombatant({
    required String name,
    required CombatantType type,
    required double initiative,
  }) {
    var combatant = Combatant(
      combat: this,
      name: name,
      type: type,
      initiative: initiative,
    );
    _combatants.add(combatant);

    notifyListeners();
  }

  void removeCombatant(Combatant combatant) {
    if (this != combatant.combat) {
      throw 'Cannot remove other combat\'s combatant.';
    }

    // remove all of the effects associated with this combatatant

    _effects
        .where(
          (ef) => ef.byCombatant == combatant || ef.onCombatant == combatant,
        )
        .forEach(
          (ef) => _effects.remove(ef),
        );

    if (_combatants.length > 1) {
      final ci = _combatants.indexOf(combatant);
      _combatants.removeAt(ci);
      if (_currentCombatant == combatant) {
        // wrap arround for end condition
        final iCurrent = ci % _combatants.length;
        _currentCombatant = _combatants[iCurrent];
        // we just removed the currentCombatant,
        // so it must not be in its turn any longer
        _inTurn = null;
      }
    } else {
      _currentCombatant = null;
      _combatants.clear();
      // we just removed the currentCombatant,
      // so it must not be in its turn any longer
      _inTurn = null;
    }

    // remove combatant from delayedCombatants, if there
    _delayedCombatants.remove(combatant);

    // tell combatant it's been removed
    combatant.removed();

    notifyListeners();
  }

  void beginTurn() {
    if (_combatants.isEmpty) {
      return;
    }

    if (_currentCombatant == null) {
      _currentCombatant = _combatants[0];
      ++_round;
    }

    // if the currentCombatant is delayed, then it is no longer delayed
    if (_delayedCombatants.contains(_currentCombatant)) {
      _delayedCombatants.remove(_currentCombatant);
      _currentCombatant?.undelay();
      _currentCombatant?.endTurn();
    }

    // begin the combatant's turn
    _inTurn = _currentCombatant;
    _currentCombatant?.beginTurn();

    notifyListeners();
  }

  void endTurn() {
    if (_currentCombatant == null) {
      _inTurn = null;
      return;
    }

    // end the combatant's turn
    _inTurn = null;
    _currentCombatant?.endTurn();

    _nextCurrentCombatant();

    notifyListeners();
  }

  // delay is an alternative to endTurn
  void delay() {
    if (_currentCombatant == null) {
      _inTurn = null;
      return;
    }

    _delayedCombatants.add(_currentCombatant!);

    // tell the current combatant it is delayed
    _currentCombatant?.delay();

    _inTurn = null;
    _nextCurrentCombatant();

    notifyListeners();
  }

  // undelay is an alternative to beginTurn
  void undelay(Combatant delayedCombatant) {
    if (!_delayedCombatants.contains(delayedCombatant)) {
      throw 'Combatant, ${delayedCombatant.name}, not contained in delayed list.';
    }

    // remove the delayedCombatant from the delayed combatants list
    _delayedCombatants.remove(delayedCombatant);

    if (_combatants.length > 1) {
      // move the delayed combatant to right before the current combatant

      _combatants.remove(delayedCombatant);

      if (_currentCombatant == null) {
        _currentCombatant = _combatants[0];
      }

      final ci = _combatants.indexOf(_currentCombatant!);
      _combatants.insert(ci, delayedCombatant);
    }

    _currentCombatant = delayedCombatant;

    // we don't need to redo the delayed combatant's beginTurn
    // that's already been done before it was delayed in the first place
    delayedCombatant.undelay();

    _inTurn = delayedCombatant;

    notifyListeners();
  }

  void _nextCurrentCombatant() {
    if (_currentCombatant == null) {
      throw 'Cannot find next turn with no current Combatant';
    }

    var ci = _combatants.indexOf(_currentCombatant!);

    // choose next currentCombatant, whose turn is about to begin
    ci = (ci + 1) % _combatants.length;
    _currentCombatant = _combatants[ci];

    // check if we are at the start of a new round
    if (ci == 0) {
      ++_round;
    }
  }
}
