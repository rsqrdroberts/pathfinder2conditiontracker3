import 'package:flutter/foundation.dart';
import 'package:uuid/uuid.dart';

import './combatant_listener.dart';
import './combatant.dart';
import './duration.dart';

var _globalUuid = Uuid();

void _invoke(CombatantListenerCallback? cb, Combatant c) {
  if (cb == null) {
    return;
  }
  cb(c);
}

// The Effect class handles the toggling of the associated
// combatants beginning their turns

abstract class Effect with ChangeNotifier {
  final String _uuid;
  final Combatant? _onCombatant;
  final Combatant? _byCombatant;
  final List<CombatantListener> _onCombatantListeners = [];
  final List<CombatantListener> _byCombatantListeners = [];
  Duration? _remaining;
  int? _value;

  bool _onHasBegunTurn = false;
  bool _byHasBegunTurn = false;

  Effect({
    Combatant? onCombatant,
    Combatant? byCombatant,
    Duration? remaining,
    int? value,
  })  : _onCombatant = onCombatant,
        _byCombatant = byCombatant,
        _remaining = remaining,
        _value = value,
        _uuid = _globalUuid.v4() {
    if (onCombatant == null && byCombatant == null) {
      throw 'Either onCombatant or byCombatant must be non-null.';
    }
  }

  Effect.setState({
    required String uuid,
    Combatant? onCombatant,
    Combatant? byCombatant,
    Duration? remaining,
    int? value,
    bool onHasBegunTurn = false,
    bool byHasBegunTurn = false,
  })  : _onCombatant = onCombatant,
        _byCombatant = byCombatant,
        _remaining = remaining,
        _value = value,
        _uuid = uuid,
        _onHasBegunTurn = onHasBegunTurn,
        _byHasBegunTurn = byHasBegunTurn {
    if (onCombatant == null && byCombatant == null) {
      throw 'Either onCombatant or byCombatant must be non-null.';
    }
  }

  Combatant? get onCombatant => _onCombatant;
  Combatant? get byCombatant => _byCombatant;

  String get uuid => _uuid;

  Duration? get remaining => _remaining;
  set remaining(Duration? rem) {
    _remaining = rem;
    notifyListeners();
  }

  int? get value => _value;
  set value(int? val) {
    _value = val;
    notifyListeners();
  }

  bool get onHasBegunTurn => _onHasBegunTurn;
  bool get byHasBegunTurn => _byHasBegunTurn;

  void addCombatantListeners({
    CombatantListener? onCL,
    CombatantListener? byCL,
  }) {
    if (_onCombatant != null && onCL != null) {
      _onCombatantListeners.add(onCL);
      _onCombatant?.addCombatantListener(onCL);
    }
    if (_byCombatant != null && byCL != null) {
      _byCombatantListeners.add(byCL);
      _byCombatant?.addCombatantListener(byCL);
    }
  }

  void added() {
    addCombatantListeners(
        onCL: CombatantListener(
          onBeginTurn: (_) => _onHasBegunTurn = true,
        ),
        byCL: CombatantListener(
          onBeginTurn: (_) => _byHasBegunTurn = true,
        ));
  }

  void removed() {
    _onCombatantListeners.forEach(
      (cl) => _onCombatant?.removeCombatantListener(cl),
    );

    _byCombatantListeners.forEach(
      (cl) => _byCombatant?.removeCombatantListener(cl),
    );
  }
}
