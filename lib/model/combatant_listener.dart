import './combatant.dart';

typedef CombatantListenerCallback = void Function(Combatant c);

// TURNS, CRB pp. 468-469
// DELAY, CRB p. 470
class CombatantListener {
  final CombatantListenerCallback? onBeginTurn;
  final CombatantListenerCallback? onEndTurn;
  final CombatantListenerCallback? onDelayed;
  final CombatantListenerCallback? onUndelayed;

  final CombatantListenerCallback? onTriggerAtBegin;
  final CombatantListenerCallback? onDecrementRemainingAtBegin;
  final CombatantListenerCallback? onRecoveryCheckAtBegin;
  final CombatantListenerCallback? onRegenerationAtBegin;

  final CombatantListenerCallback? onTriggerAtEnd;
  final CombatantListenerCallback? onEndEffectsAtEnd;
  final CombatantListenerCallback? onDecrementRemainingAtEnd;
  final CombatantListenerCallback? onPersistentDamageAtEnd;
  final CombatantListenerCallback? onAfflictionSaveAtEnd;
  final CombatantListenerCallback? onConditionChangeAtEnd;

  final CombatantListenerCallback? onEndBeneficalEffectsAtDelay;
  final CombatantListenerCallback? onPersistentDamageAtDelay;
  final CombatantListenerCallback? onNegativeEffectsAtDelay;

  CombatantListener({
    this.onBeginTurn,
    this.onEndTurn,
    this.onDelayed,
    this.onUndelayed,
    this.onTriggerAtBegin,
    this.onDecrementRemainingAtBegin,
    this.onRecoveryCheckAtBegin,
    this.onRegenerationAtBegin,
    this.onTriggerAtEnd,
    this.onEndEffectsAtEnd,
    this.onDecrementRemainingAtEnd,
    this.onAfflictionSaveAtEnd,
    this.onConditionChangeAtEnd,
    this.onPersistentDamageAtEnd,
    this.onPersistentDamageAtDelay,
    this.onEndBeneficalEffectsAtDelay,
    this.onNegativeEffectsAtDelay,
  });
}
