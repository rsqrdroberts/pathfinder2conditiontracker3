class Duration {
  int _rounds;
  static const int SECONDS_PER_ROUND = 6;
  static const int ROUNDS_PER_MINUTE = 10;
  static const int ROUNDS_PER_HOUR = 60 * ROUNDS_PER_MINUTE;
  static const int ROUNDS_PER_DAY = 24 * ROUNDS_PER_HOUR;

  Duration(
    int rounds,
  ) : _rounds = rounds {
    if (_rounds < 0) {
      throw "Duration must be non-negative.";
    }
  }

  Duration.zero() : _rounds = 0;

  Duration.dhmr([int d = 0, int h = 0, int m = 0, int r = 0])
      : _rounds = r +
            ROUNDS_PER_MINUTE * m +
            ROUNDS_PER_HOUR * h +
            ROUNDS_PER_DAY * d {
    if (_rounds < 0) {
      throw "Duration must be non-negative.";
    }
  }

  bool get isExpired => _rounds == 0;

  int get rounds => _rounds;

  int get days => _rounds ~/ ROUNDS_PER_DAY;

  int get hoursOfDay => _rounds.remainder(ROUNDS_PER_DAY) ~/ ROUNDS_PER_HOUR;

  int get minutesOfHour =>
      _rounds.remainder(ROUNDS_PER_HOUR) ~/ ROUNDS_PER_MINUTE;

  int get roundsOfMinute => _rounds.remainder(ROUNDS_PER_MINUTE);

  Duration copy() {
    return Duration(_rounds);
  }

  Duration diff(Duration d) {
    return this - d._rounds;
  }

  Duration operator -(int rounds) {
    var r = _rounds - rounds;
    return Duration(r > 0 ? r : 0);
  }

  Duration operator +(int rounds) {
    var r = _rounds + rounds;
    return Duration(r);
  }

  @override
  bool operator ==(dynamic other) {
    return other is Duration && other._rounds == _rounds;
  }

  // Override hashCode using strategy from Effective Java,
  // Chapter 11.
  @override
  int get hashCode {
    int result = 17;
    result = 37 * result + _rounds.hashCode;
    return result;
  }
}
